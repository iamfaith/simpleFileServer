const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('./config/define.js');

app.use(fileUpload());
app.use(express.static(path.frontDir));
var routes = require('./routes')(app);
app.listen(8000, () => console.log('Example app listening on port 8000!'))
