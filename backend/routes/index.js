const fs = require('fs');
const exec = require('exec');
const path = require('../config/define');
const util = require('../util/fileUtil');
module.exports = function (app) {


    app.post('/upload', function (req, res) {
        "use strict";
        if (!req.files)
            return res.status(400).send('No files were uploaded.');
        let sampleFile = req.files.sampleFile;
        let fileBackup = path.postFolder + sampleFile.name;
        console.log(sampleFile);
        sampleFile.mv(fileBackup, function (err) {
            if (err)
                return res.status(500).send(err);
            res.send('File uploaded!');
            exec('sudo ./generate.sh',
                function (err, out, code) {
                    process.stderr.write(err);
                    process.stdout.write(out);
                });
        });
    });

    app.get('/posts', function (req, resp) {
        //console.log(path);
        fs.readdir(path.postFolder, (err, files) => {
            if (err instanceof Error) {
                console.log(err);
                resp.send("uhh something error~~");
            } else {
                const arr = [];
                console.log(util);
                files.forEach(file => {
                    arr.push({
                        szie: util.getFilesizeInBytes(file) / 1000 + "KB", name: file
                    });
                });
                console.log(arr);
                resp.send(JSON.stringify(arr));
            }
        });
    });
}