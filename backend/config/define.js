module.exports = (function () {
    const debug = true;
    var postFolder = '';
    if (debug) {
         postFolder = './';
    } else {
         postFolder = '/home/faith/hexoblog/blog.io/source/_posts/';
    }
    const frontDir = '../frontend/';
    return {
        postFolder: postFolder,
        frontDir: frontDir
    }
})()