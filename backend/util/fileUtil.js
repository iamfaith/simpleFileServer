/**
 * Created by faith on 2018/1/12.
 */
const fs = require('fs');
exports.getFilesizeInBytes = function (filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}